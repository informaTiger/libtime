/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.time;

/**
 *
 * @author thomas
 */
public class Time {
    
    public static final int FACTOR_HOURS = 3600000;
    
    public static final int FACTOR_MINUTES = 60000;
    
    public static final int FACTOR_SECONDS = 1000;
    
    public static final int MAX_SECONDS = 60;
    
    public static final int MAX_MINUTES = 60;
    
    public static final int MAX_HOURS = 24;
    
    private int hours;
    
    private int minutes;
    
    private int seconds;
    
    public Time(){
        
    }
    
    public Time(long millis){
        hours = (int)millis / FACTOR_HOURS;
        millis -= hours * FACTOR_HOURS;
        
        minutes = (int)millis / FACTOR_MINUTES;
        millis -= minutes * FACTOR_MINUTES;
        
        seconds = (int)millis / FACTOR_SECONDS;
    }
    
    public Time(int hours, int minutes, int seconds){
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
    
    public boolean isZero(){
        return hours == 0 &&
               minutes == 0 &&
               seconds == 0;
    }
    
    public void addSeconds(int seconds){
        while (seconds > MAX_SECONDS){
            addMinutes(1);
            seconds -= MAX_SECONDS;
        }
        this.seconds += seconds;
    }
    
    public void addMinutes(int minutes){
        while (minutes > MAX_MINUTES){
            addHours(1);
            minutes -= MAX_MINUTES;
        }
        this.minutes += minutes;
    }
    
    public void addHours(int hours){
        if (this.hours > MAX_HOURS){
            throw new IllegalArgumentException("Time has covered a full day. Can not add more hours");
        }
        this.hours += hours;
    }
    
    public void subtractSeconds(int seconds){
        if (this.seconds == 0){
            subtractMinutes(1);
            this.seconds = MAX_SECONDS;
        }
        
        while (seconds > MAX_SECONDS){
            subtractMinutes(1);
            seconds -= MAX_SECONDS;
        }
        this.seconds -= seconds;
    }
    
    public void subtractMinutes(int minutes){
        if (this.minutes == 0){
            subtractHours(1);
            this.minutes = MAX_MINUTES;
        }
        
        while (minutes > MAX_MINUTES){
            subtractHours(1);
            minutes -= MAX_MINUTES;
        }
        this.minutes -= minutes;
    }
    
    public void subtractHours(int hours){
        if (this.hours == 0){
            return;
        }
        this.hours -= hours;
    }
            
    public long toMillis(){
        return hours * FACTOR_HOURS +
               minutes * FACTOR_MINUTES +
               seconds * FACTOR_SECONDS;
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
    
    public static Time parse(String time){
        String[] parts = time.split(":");
        
        int hours = Integer.parseInt(parts[0]);
        int minutes = Integer.parseInt(parts[1]);
        int seconds = Integer.parseInt(parts[2]);
        
        return new Time(hours, minutes, seconds);
    }
}